package com.br.mastertech.fatura.clients.cartao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Cartão inativo")
public class CartaoInativoException extends RuntimeException {

}
