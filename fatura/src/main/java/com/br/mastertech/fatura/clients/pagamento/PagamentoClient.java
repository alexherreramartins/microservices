package com.br.mastertech.fatura.clients.pagamento;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "PAGAMENTO", configuration = PagamentoClientConfiguration.class)
public interface PagamentoClient {

    @GetMapping("/payments/{id}")
    List<Pagamento> getByCartaoId(@PathVariable int id);

    @DeleteMapping("/payments/{id}")
    void deleteAllById(@PathVariable int id);
}
