package com.br.mastertech.fatura.clients.cartao;

import com.br.mastertech.fatura.dto.ExpirarCartaoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cards/{id}")
    Cartao getById(@PathVariable int id);

    @PostMapping("/cards/{id}/expirar")
    ExpirarCartaoResponse updateStatusCartao(@PathVariable int id);
}
