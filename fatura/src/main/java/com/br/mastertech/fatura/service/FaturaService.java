package com.br.mastertech.fatura.service;

import com.br.mastertech.fatura.clients.cartao.Cartao;
import com.br.mastertech.fatura.clients.cartao.CartaoClient;
import com.br.mastertech.fatura.clients.cliente.Cliente;
import com.br.mastertech.fatura.clients.cliente.ClienteClient;
import com.br.mastertech.fatura.clients.pagamento.Pagamento;
import com.br.mastertech.fatura.clients.pagamento.PagamentoClient;
import com.br.mastertech.fatura.dto.ExpirarCartaoResponse;
import com.br.mastertech.fatura.dto.FaturaMapper;
import com.br.mastertech.fatura.exception.ClienteNotOwnerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private CartaoClient cartaoClient;

    @Autowired
    private PagamentoClient pagamentoClient;

    @Autowired
    private FaturaMapper faturaMapper;

    public List<Pagamento> getFatura(int idCliente, int idCartao) {
            Cliente cliente = clienteClient.getById(idCliente);
            Cartao cartao = cartaoClient.getById(idCartao);

            if (cartao.getClienteId() == idCliente) {
                return pagamentoClient.getByCartaoId(idCartao);
            }

            throw new ClienteNotOwnerException();
    }

    public void delete(int clienteId, int cartaoId) {
        Cliente cliente = clienteClient.getById(clienteId);
        Cartao cartao = cartaoClient.getById(cartaoId);

        if (cartao.getClienteId() == clienteId) {
            pagamentoClient.deleteAllById(cartaoId);
        } else {
            throw new ClienteNotOwnerException();
        }

    }

    public ExpirarCartaoResponse update(int clienteId, int cartaoId) {
            Cliente cliente = clienteClient.getById(clienteId);
            Cartao cartao = cartaoClient.getById(cartaoId);

            if (cartao.getClienteId() == clienteId) {
                cartao.setAtivo(false);
                return cartaoClient.updateStatusCartao(cartaoId);
            } else {
                throw new ClienteNotOwnerException();
            }

    }
}
