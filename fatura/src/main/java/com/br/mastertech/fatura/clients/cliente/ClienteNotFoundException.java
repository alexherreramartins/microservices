package com.br.mastertech.fatura.clients.cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Cliente não cadastrado")
public class ClienteNotFoundException extends RuntimeException {

}
