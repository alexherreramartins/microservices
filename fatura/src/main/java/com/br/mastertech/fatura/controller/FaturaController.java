package com.br.mastertech.fatura.controller;

import com.br.mastertech.fatura.clients.pagamento.Pagamento;
import com.br.mastertech.fatura.dto.ExpirarCartaoResponse;
import com.br.mastertech.fatura.dto.FaturaMapper;
import com.br.mastertech.fatura.dto.PagarFaturaResponse;
import com.br.mastertech.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @Autowired
    private FaturaMapper faturaMapper;

    @GetMapping("/{cliente-id}/{cartao-id}")
    public List<Pagamento> getFaturaByIdClienteCartao(@PathVariable(name = "cliente-id") int clienteId,
                                                      @PathVariable(name = "cartao-id") int cartaoId) {
            return faturaService.getFatura(clienteId, cartaoId);
    }

    @PostMapping("/{cliente-id}/{cartao-id}/pagar")
    public PagarFaturaResponse pagarFatura(@PathVariable(name = "cliente-id") int clienteId,
                                           @PathVariable(name = "cartao-id") int cartaoId) {
            List<Pagamento> fatura = faturaService.getFatura(clienteId, cartaoId);

            faturaService.delete(clienteId, cartaoId);

            return faturaMapper.toPagarFaturaResponse(fatura, cartaoId);
    }


    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public ExpirarCartaoResponse expirarCartaoParaFatura(@PathVariable(name = "cliente-id") int clienteId,
                                             @PathVariable(name = "cartao-id") int cartaoId) {
            return  faturaService.update(clienteId, cartaoId);
     }
}
