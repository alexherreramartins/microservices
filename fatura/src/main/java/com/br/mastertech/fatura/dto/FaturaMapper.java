package com.br.mastertech.fatura.dto;

import com.br.mastertech.fatura.clients.cartao.Cartao;
import com.br.mastertech.fatura.clients.pagamento.Pagamento;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class FaturaMapper {

    public PagarFaturaResponse toPagarFaturaResponse(List<Pagamento> pagamentoList, int cartaoId){

        Double valorFinal = 0.0;
        PagarFaturaResponse pagarFaturaResponse =  new PagarFaturaResponse();

        for (Pagamento pagamento : pagamentoList){
            valorFinal += pagamento.getValor();
        }

        pagarFaturaResponse.setId(cartaoId);
        pagarFaturaResponse.setPagoEm(new Date(System.currentTimeMillis()));
        pagarFaturaResponse.setValorPago(valorFinal);

        return pagarFaturaResponse;
    }

    public ExpirarCartaoResponse toExpirarCartaoResponse(Cartao cartao){

        ExpirarCartaoResponse expirarCartaoResponse = new ExpirarCartaoResponse();

        if (!cartao.isAtivo()) {
            expirarCartaoResponse.setStatus("ok");
        }

        return expirarCartaoResponse;
    }

    public ExpirarCartaoRequest toExpirarCartaoRequestClient(Cartao cartao) {
        ExpirarCartaoRequest expirarCartaoRequest = new ExpirarCartaoRequest();
        expirarCartaoRequest.setAtivo(cartao.isAtivo());

        return expirarCartaoRequest;
    }
}
