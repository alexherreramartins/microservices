package com.br.mastertech.fatura.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Cliente não é dono deste cartão.")
public class ClienteNotOwnerException extends RuntimeException {
}
