package com.br.mastertech.fatura.clients.cliente;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getClienteClientDecoder(){
        return new ClienteClientDecoder();
    }

}
