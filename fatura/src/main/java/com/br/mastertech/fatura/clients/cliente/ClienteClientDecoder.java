package com.br.mastertech.fatura.clients.cliente;

import com.br.mastertech.fatura.clients.cartao.CartaoInativoException;
import com.br.mastertech.fatura.clients.cartao.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404){
            return new ClienteNotFoundException();
        }

        return errorDecoder.decode(s,response);
    }
}
