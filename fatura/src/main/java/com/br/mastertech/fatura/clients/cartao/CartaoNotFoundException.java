package com.br.mastertech.fatura.clients.cartao;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Cartão não cadastrado")
public class CartaoNotFoundException extends RuntimeException {

}
