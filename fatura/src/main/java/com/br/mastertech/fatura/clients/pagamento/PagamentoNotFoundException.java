package com.br.mastertech.fatura.clients.pagamento;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Pagamento não encontrado para o cartao.")
public class PagamentoNotFoundException extends RuntimeException {

}
