package com.br.mastertech.fatura.dto;

public class ExpirarCartaoRequest {
    private boolean ativo;

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
