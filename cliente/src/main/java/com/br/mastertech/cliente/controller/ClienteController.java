package com.br.mastertech.cliente.controller;

import com.br.mastertech.cliente.model.Cliente;
import com.br.mastertech.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/clients")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente createCliente(@RequestBody Cliente cliente ){
        return clienteService.createCliente(cliente);
    }

    @GetMapping("/{id}")
    public Optional<Cliente> getCliente(@PathVariable(name = "id") int id ){
        return clienteService.findClienteById(id);
    }
}
