package com.br.mastertech.cliente.service;

import com.br.mastertech.cliente.exceptions.ClienteNotFoundException;
import com.br.mastertech.cliente.model.Cliente;
import com.br.mastertech.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Optional<Cliente> findClienteById(int id){
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if (!cliente.isPresent()){
            throw new ClienteNotFoundException();
        }
        return clienteRepository.findById(id);
    }

    public Cliente createCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }
}
