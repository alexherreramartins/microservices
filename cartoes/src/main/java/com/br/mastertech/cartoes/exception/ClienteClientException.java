package com.br.mastertech.cartoes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientException extends RuntimeException {
    public ClienteClientException(String message) {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
    }
}
