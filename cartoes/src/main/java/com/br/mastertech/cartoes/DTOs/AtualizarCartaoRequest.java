package com.br.mastertech.cartoes.DTOs;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AtualizarCartaoRequest {

    private boolean ativo;

    public AtualizarCartaoRequest() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
