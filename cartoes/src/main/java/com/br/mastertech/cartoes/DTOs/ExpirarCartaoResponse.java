package com.br.mastertech.cartoes.DTOs;

public class ExpirarCartaoResponse {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
