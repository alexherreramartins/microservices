package com.br.mastertech.cartoes;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;


public class RibbonConfiguration {

    @Bean
    public IRule getRule() {
        return new RandomRule();
    }

}
