package com.br.mastertech.cartoes.DTOs;

import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.client.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CriarCartaoRequest criarCartaoRequest)
    {
        Cliente cliente = new Cliente();
        cliente.setId(criarCartaoRequest.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(criarCartaoRequest.getNumero());
        cartao.setIdCliente(criarCartaoRequest.getClienteId());

        return cartao;
    }

    public CriarCartaoResponse toCriarCartaoResponse(Cartao cartao) {

        CriarCartaoResponse criarCartaoResponse = new CriarCartaoResponse();

        criarCartaoResponse.setAtivo(cartao.isAtivo());
        criarCartaoResponse.setClienteId(cartao.getIdCliente());
        criarCartaoResponse.setId(cartao.getId());
        criarCartaoResponse.setNumero(cartao.getNumero());

        return criarCartaoResponse;
    }

    public ConsultarCartaoResponse toConsultarCartaoResponse(Cartao cartao) {

        ConsultarCartaoResponse consultarCartaoResponse = new ConsultarCartaoResponse();

        consultarCartaoResponse.setClienteId(cartao.getIdCliente());
        consultarCartaoResponse.setId(cartao.getId());
        consultarCartaoResponse.setNumero(cartao.getNumero());

        return consultarCartaoResponse;
    }

    public Cartao toCartao(AtualizarCartaoRequest atualizarCartaoRequest,int id)
    {
        Cartao cartao = new Cartao();
        cartao.setId(id);
        cartao.setAtivo(atualizarCartaoRequest.isAtivo());

        return cartao;
    }

    public ExpirarCartaoResponse toExpirarCartaoResponse(Cartao cartao) {
        ExpirarCartaoResponse expirarCartaoResponse = new ExpirarCartaoResponse();
        if (!cartao.isAtivo()){
            expirarCartaoResponse.setStatus("ok");
        }else{
            expirarCartaoResponse.setStatus("erro ao expirar cartao.");
        }
        return expirarCartaoResponse;
    }
}
