package com.br.mastertech.cartoes.services;

import com.br.mastertech.cartoes.client.Cliente;
import com.br.mastertech.cartoes.client.ClienteClient;
import com.br.mastertech.cartoes.exception.CartaoInativoException;
import com.br.mastertech.cartoes.exception.CartaoNotFoundException;
import com.br.mastertech.cartoes.exception.ClienteClientException;
import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteClient clienteClient;

    public Cartao create(Cartao cartao) {
        Cliente cliente = clienteClient.getById(cartao.getIdCliente());

        cartao.setIdCliente(cliente.getId());
        cartao.setNumero(cartao.getNumero());


        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao cartao) {

        Optional<Cartao> cartaoDB = cartaoRepository.findById(cartao.getId());

        if (cartaoDB.isPresent()) {
            cartaoDB.get().setAtivo(cartao.isAtivo());

            return cartaoRepository.save(cartaoDB.get());
        }
        throw new CartaoNotFoundException();
    }

    public Cartao findCartaoById(int id) {

        Optional<Cartao> cartao = cartaoRepository.findById(id);

        if (cartao.isPresent()) {
            if (cartao.get().isAtivo()) {
                return cartao.get();
            }
            throw new CartaoInativoException();
        }
        throw new CartaoNotFoundException();

    }
}
