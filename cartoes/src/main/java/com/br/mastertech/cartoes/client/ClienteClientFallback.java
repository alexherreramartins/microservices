package com.br.mastertech.cartoes.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente getById(int id) {
       throw new ResponseStatusException(HttpStatus.GATEWAY_TIMEOUT, "Microserviço de clientes fora!");
    }
}
