package com.br.mastertech.cartoes.controllers;

import com.br.mastertech.cartoes.DTOs.*;
import com.br.mastertech.cartoes.models.Cartao;
import com.br.mastertech.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@RequestMapping("/cards")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CriarCartaoResponse createCartao(@RequestBody @Valid CriarCartaoRequest criarCartaoRequest) {
        Cartao cartao = cartaoMapper.toCartao(criarCartaoRequest);
        cartao = cartaoService.create(cartao);

        return cartaoMapper.toCriarCartaoResponse(cartao);
    }

    @PatchMapping("/{id}")
    public CriarCartaoResponse updateStatusCartao(@RequestBody @Valid AtualizarCartaoRequest atualizarCartaoRequest, @PathVariable(name = "id") int id) {

        Cartao cartao = cartaoMapper.toCartao(atualizarCartaoRequest, id);
        cartao = cartaoService.update(cartao);

        return cartaoMapper.toCriarCartaoResponse(cartao);

    }

    @GetMapping("/{id}")
    public ConsultarCartaoResponse findCartaoById(@PathVariable(name = "id") int id) {

        Cartao cartao = cartaoService.findCartaoById(id);

        return cartaoMapper.toConsultarCartaoResponse(cartao);
    }

    @PostMapping("/{id}/expirar")
    public ExpirarCartaoResponse createCartao(@PathVariable(name = "id") int id) {
        Cartao cartao = cartaoService.findCartaoById(id);
        cartao.setAtivo(false);
        cartao = cartaoService.update(cartao);

        return cartaoMapper.toExpirarCartaoResponse(cartao);
    }
}
