package com.br.mastertech.pagamento.dto;

import com.br.mastertech.pagamento.models.Pagamento;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {
    public Pagamento toPagamento(CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = new Pagamento();
        pagamento.setIdCartao(createPagamentoRequest.getCartao_id());
        pagamento.setDescricao(createPagamentoRequest.getDescricao());
        pagamento.setValor(createPagamentoRequest.getValor());

        return pagamento;
    }

    public CreatePagamentoResponse toCreatePagamentoResponse(Pagamento pagamento) {
        CreatePagamentoResponse createPagamentoResponse = new CreatePagamentoResponse();
        createPagamentoResponse.setId(pagamento.getId());
        createPagamentoResponse.setCartao_id(pagamento.getIdCartao());
        createPagamentoResponse.setDescricao(pagamento.getDescricao());
        createPagamentoResponse.setValor(pagamento.getValor());

        return createPagamentoResponse;
    }

    public List<CreatePagamentoResponse> toListCreatePagamentoResponse(Iterable<Pagamento> pagamentosIdCartao) {

        List<CreatePagamentoResponse> createPagamentoResponseList = new ArrayList<>();

        for (Pagamento pagamento : pagamentosIdCartao) {
            createPagamentoResponseList.add(toCreatePagamentoResponse(pagamento));
        }
        return createPagamentoResponseList;
    }
}
