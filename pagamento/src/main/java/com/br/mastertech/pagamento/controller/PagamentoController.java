package com.br.mastertech.pagamento.controller;

import com.br.mastertech.pagamento.dto.CreatePagamentoRequest;
import com.br.mastertech.pagamento.dto.CreatePagamentoResponse;
import com.br.mastertech.pagamento.dto.PagamentoMapper;
import com.br.mastertech.pagamento.models.Pagamento;
import com.br.mastertech.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/payments")
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @Autowired
    PagamentoMapper pagamentoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreatePagamentoResponse createPagamento(@RequestBody @Valid CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = pagamentoService.createPagamento(pagamentoMapper.toPagamento(createPagamentoRequest));
        return pagamentoMapper.toCreatePagamentoResponse(pagamento);
    }

    @GetMapping("/{id_cartao}")
    public List<CreatePagamentoResponse> findPagamentosIdCartao(@PathVariable(name = "id_cartao") int id_cartao) {
        List<CreatePagamentoResponse> createPagamentoResponseList =
                pagamentoMapper.toListCreatePagamentoResponse(
                        pagamentoService.findPagamentosIdCartao(id_cartao));
        return createPagamentoResponseList;

    }

    @DeleteMapping("/{id_cartao}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePagamentoByIdCartao (@PathVariable(name = "id_cartao") int id_cartao) {
        pagamentoService.deletePagamentosById(id_cartao);
    }

}