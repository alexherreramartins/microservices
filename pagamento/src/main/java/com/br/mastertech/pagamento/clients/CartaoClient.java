package com.br.mastertech.pagamento.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cards/{id}")
    Cartao getById(@PathVariable int id);

}
