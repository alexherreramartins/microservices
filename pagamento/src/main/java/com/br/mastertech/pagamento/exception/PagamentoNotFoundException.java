package com.br.mastertech.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Pagamento não encontrado")
public class PagamentoNotFoundException extends RuntimeException{

}
