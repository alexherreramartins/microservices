package com.br.mastertech.pagamento.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {

    @Bean
    public ErrorDecoder getCartaoClientDecoder(){
        return new CartaoClientDecoder();
    }

}
