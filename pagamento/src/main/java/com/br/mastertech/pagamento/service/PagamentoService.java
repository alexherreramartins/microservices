package com.br.mastertech.pagamento.service;

import com.br.mastertech.pagamento.clients.Cartao;
import com.br.mastertech.pagamento.clients.CartaoClient;
import com.br.mastertech.pagamento.exception.PagamentoNotFoundException;
import com.br.mastertech.pagamento.models.Pagamento;
import com.br.mastertech.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoClient cartaoClient;

    public Pagamento createPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoClient.getById(pagamento.getIdCartao());
        return pagamentoRepository.save(pagamento);
    }

    public Iterable<Pagamento> findPagamentosIdCartao(int id_cartao) {
        List<Pagamento> pagamentoList = pagamentoRepository.findAllByIdCartao(id_cartao);

        if (pagamentoList.size() > 0) {
            return pagamentoList;
        } else {
            throw new PagamentoNotFoundException();
        }
    }

    @Transactional
    public void deletePagamentosById(int id_cartao) {
        Cartao cartao = cartaoClient.getById(id_cartao);
        pagamentoRepository.deleteAllByIdCartao(cartao.getId());

    }

}

