package com.br.mastertech.pagamento.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Cartão não cadastrado")
public class CartaoNotFoundException extends RuntimeException {

}
