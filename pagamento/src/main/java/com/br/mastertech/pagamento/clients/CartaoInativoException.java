package com.br.mastertech.pagamento.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Cartão inativo")
public class CartaoInativoException extends RuntimeException {

}
